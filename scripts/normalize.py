# Script to remove outliers and normalize data
import numpy as np
from pandas import read_csv
import math

# Read data
data = read_csv('data/tips_per_block(05.02-06.02).csv', usecols=[1])
dataset = data.values
dataset = dataset.astype('float32')
sum = 0
mean = 0
n = 0

# Compute the mean fee
for fee in dataset:
    mean = mean + fee
    n = n +1
mean ="{:.2f}".format(mean[0] / n)

# Start computing the standard deviation
for fee in dataset:
    sum = sum + (fee - float(mean))*(fee - float(mean))

# Get 1.5 the standard deviation
dev = 1.5*math.sqrt(sum/n)

# Write data
with open('data/min_tips_normalize.csv', 'w') as file:
    for fee in dataset:
        if fee < dev:
            file.write(str(fee[0]))
            file.write('\n')
