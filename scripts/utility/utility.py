from web3 import Web3
import datetime
import sys
import csv

# Dummy function for integration with tests
def add(a,b):
    return a+b
# Utility functions

# Input: block number and geth node
# Output: transactions hashes from the block
def get_transactions(block_num, node):
    if node.isConnected() == False:
        printf("Error couldn't connect to geth node")
        return 
    txs = node.eth.get_block(str(hex(int(block_num)))).get('transactions')
    new_tx = []
    for tx in txs:
        new_tx.append(tx.hex())
    return new_tx

# Input: transaction hash and geth node
# Output: gas used for computation
def get_gas(tx_hash, node):
    if node.isConnected() == False:
        printf("Error couldn't connect to geth node")
        return
    gas = node.eth.get_transaction(tx_hash).get('gas')
    return gas

# Input: transaction hash and geth node
# Output: gas price at time of transaction
def get_gas_price(tx_hash, node):
    if node.isConnected() == False:
        printf("Error couldn't connect to geth node")
        return
    gas_price = node.eth.get_transaction(tx_hash).get('gasPrice')
    return gas_price

# Input: transaction hash and geth node
# Output: maxPriorityFeePerGas -> if none is found that means tx is legacy type
def get_maxPriorityFee(tx_hash, node):
    if node.isConnected() == False:
        printf("Error couldn't connect to geth node")
        return
    fee = node.eth.get_transaction(tx_hash).get('maxPriorityFeePerGas')
    if fee is None:
        raise RuntimeError("Could not get maxPriorityFeePerGas")
        return
    return fee

# Input: transaction hash and geth node
# Output: maxFeePerGas (maxFee) -> if none is found that means tx is legacy type
def get_maxFee(tx_hash, node):
    if node.isConnected() == False:
        printf("Error couldn't connect to geth node")
        return
    fee = node.eth.get_transaction(tx_hash).get('maxFeePerGas')
    if fee is None:
        raise RuntimeError("Could not get maxFeePerGas")
        return
    return fee

# Input: block number and geth node
# Output: timestamp in UNIX
def get_block_timestamp(block_num, node):
    if node.isConnected() == False:
        printf("Error couldn't connect to geth node")
        return
    timestamp = node.eth.get_block(str(hex(int(block_num)))).get('timestamp')
    return int(timestamp)
    # return datetime.datetime.fromtimestamp(int(timestamp))

# Input: tx_hash and geth node
# Output: block number of that transaction
def get_block_num_from_tx(tx_hash, node):
    if node.isConnected() == False:
        printf("Error couldn't connect to geth node")
        return
    block_num = node.eth.get_transaction(tx_hash).get('blockNumber')
    return block_num

def get_base_fee(block_num, node):
    if node.isConnected() == False:
        printf("Error couldn't connect to geth node")
        return
    base_fee = node.eth.get_block(str(hex(int(block_num)))).get('baseFeePerGas')
    return int(base_fee)

def get_block_gas_used(block_num, node):
    if node.isConnected() == False:
        printf("Error couldn't connect to geth node")
        return
    gas_used = node.eth.get_block(str(hex(int(block_num)))).get('gasUsed')
    return gas_used

def write_tx(gas, gas_price, max_priority, max_fee, pending_time):
    fieldnames = ['gasUsed', 'gasPrice', 'maxPriorityFee', 'maxFee', 'txTime']
    with open('data/data.csv', 'a') as file:
            csv_writer = csv.DictWriter(file, fieldnames=fieldnames)
            csv_writer.writerow({'gasUsed':gas, 'gasPrice': gas_price,
                                 'maxPriorityFee': max_priority, 'maxFee': max_fee, 'txTime': pending_time})
                            



