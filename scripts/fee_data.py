# Script collecting maxPriorityFeePerGas data from blocks

import utility.utility as utility
from web3 import Web3
import csv

# Connect with the hosted local Ethereum node
w3 = Web3(Web3.HTTPProvider('http://127.0.0.1:8545'))

if w3.isConnected() == False:
        printf("Error couldn't connect to geth node")

# Block numbers from which to extract data
block_start = 14142695
block_end   = 14149085

block_num = block_start

# list for the base fees
base_fees = []

# list for the average tip per block
average_tips = []

# list for the minimum tip per block
minium_tips = []

# list for the average of certain percent tx per block
percentile_tips = []

while block_num <= block_end:
        txs = utility.get_transactions(block_num, w3)

        # Help list
        tips = []

        # Get transactions and extract maxPriorityFeePerGas
        num_tx = 0
        for tx in txs:
                try:
                        max_fee = utility.get_maxPriorityFee(tx, w3)/1000000000
                        tips.append(max_fee)
                        num_tx = num_tx + 1
                except RuntimeError as e:
                        continue

        if num_tx == 0:
                print('Procesed block {}'.format(block_num))
                block_num = block_num + 1
                continue

        # Sort the tips 
        tips.sort()

        # Append the minimum tip
        minium_tips.append(tips[0])

        # Collect the average fee from the 5% transactions with the least value 
        percentile_num = int((len(tips)*5)/100)
        if percentile_num == 0:
            percentile_num = 1
        percentile_sum = 0
        for i in range(percentile_num):
            percentile_sum = percentile_sum + tips[i]
        percentile_tips.append(percentile_sum/percentile_num)

        # Collect the average maxPriorityFeePerGas
        average_sum = 0
        for i in range(len(tips)):
            average_sum = average_sum + tips[i]
        average_tips.append(average_sum/len(tips))
        
        print('Procesed block {}'.format(block_num))
        block_num = block_num + 1

# Compute the average 5% percentile maxPriorityFeePerGas
average_percentile = 0
if len(percentile_tips) != 0:
        for tip in percentile_tips:
                average_percentile = average_percentile + tip
        average_percentile = average_percentile/len(percentile_tips)

# Compute the average maxPriorityFeePerGas
overall_average_tip = 0
if len(average_tips) != 0:
        for tip in average_tips:
                overall_average_tip = overall_average_tip + tip
        overall_average_tip = overall_average_tip/len(average_tips)

# Write data
fieldnames1 = ['minimumTip','minimumPercentileTip','averageTip']
fieldnames2 = ['average_min_tip','average_tip']

with open('data/tips_per_block(05.02-06.02).csv', 'w') as file:
        csv_writer = csv.DictWriter(file, fieldnames=fieldnames1)
        csv_writer.writeheader()
        for i in range(len(average_tips)):
                csv_writer.writerow({'minimumTip':"{:.2f}".format(minium_tips[i]),'minimumPercentileTip':"{:.2f}".format(percentile_tips[i]),
                'averageTip':"{:.2f}".format(average_tips[i])})
with open('data/average_tips_period(05.02-06.02).csv', 'w') as file:
        csv_writer = csv.DictWriter(file, fieldnames=fieldnames2)
        csv_writer.writeheader()
        csv_writer.writerow({'average_min_tip':"{:.2f}".format(average_percentile), 'average_tip':"{:.2f}".format(overall_average_tip)})