# Script used for plotting data

import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import math
tips = read_csv('data/min_tips_normalize.csv', usecols=[0])

base_fee = read_csv('data/baseFee(05.02-06.02).csv', usecols=[0])

print(base_fee.min())
print(base_fee.max())


plt.xlabel("Block", fontsize = 14)
plt.ylabel("min maxPriorityFeePerGas in Gwei", fontsize=14)
# plt.plot(1, 20, "-b", label="Collected data")
# plt.plot(1, 19, "orange", label="Prediction on train data")
# plt.plot(1, 18, "-g", label="Prediction on test data")
# plt.legend(loc="upper left")
plt.plot(tips)
plt.show()