# Test script for utility class
from web3 import Web3
import datetime
import sys
import utility.utility as utility
class TestBasic:
    block_str = '14381719'
    block_int = 14381719
    tx1 = '0xb05b48b5c47883d299d3fd8dfdf687f92787a3119850f46ef128cf809e7a731d'
    tx2 = '0x9c5346e0aba1cb493034306ed050c86204417e03a348c78e48219deada389724'
    w3 = Web3(Web3.HTTPProvider('http://127.0.0.1:8545'))
    def test_add(self):
        assert utility.add(3,5) == 8
    def test_get_transactions(self):
        txs = utility.get_transactions(self.block_str, self.w3)
        assert '0x9c5346e0aba1cb493034306ed050c86204417e03a348c78e48219deada389724' in txs
        assert '0x597f933f4d44963a2b061e37bd1ea429806b2620cbcc5e7cab7e793d9e6c905e' in txs
        assert '0xb05b48b5c47883d299d3fd8dfdf687f92787a3119850f46ef128cf809e7a731d' in txs
    def test_get_gas(self):
        assert 1000000 == utility.get_gas(self.tx1, self.w3)
    def test_get_gas_price(self):
        assert 36673680200 == utility.get_gas_price(self.tx1, self.w3)
        #Dont forget try catch for exceptions
    def test_get_max_priority_fee(self):
        assert 2471255579 == utility.get_maxPriorityFee(self.tx2, self.w3)
    def test_get_max_fee(self):
        assert 48102421754 == utility.get_maxFee(self.tx2, self.w3)
    def test_get_block_timestamp(self):
        assert 1647220523 == utility.get_block_timestamp(self.block_int, self.w3)
    def test_get_block_num_from_tx(self):
        assert 14381719 == utility.get_block_num_from_tx(self.tx1, self.w3)
    def test_get_base_fee(self):
        assert 35196376330 == utility.get_base_fee(self.block_int, self.w3)
    def test_write_tx(self):
        assert True

