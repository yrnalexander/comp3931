# Script collecting block data regarding the baseFeePerGas

import utility.utility as utility
from web3 import Web3
import csv

# Connect with the hosted local Ethereum node
w3 = Web3(Web3.HTTPProvider('http://127.0.0.1:8545'))

if w3.isConnected() == False:
        printf("Error couldn't connect to geth node")


# Block numbers from which to extract data

block_start = 14142695
block_end = 14148085

# block_start = 14413495
# block_end = 14458510

block_num = block_start

# List for the base fee and gas used
base_fees = []
gas_used = []

while block_num <= block_end:

        # Get base fee
        base_fee = utility.get_base_fee(block_num, w3)/1000000000
        gas = utility.get_block_gas_used(block_num, w3)/1000000

        if base_fee == 0:
            continue     
        base_fees.append(base_fee)
        gas_used.append(gas)
        print('Procesed block {}'.format(block_num))
        block_num = block_num + 1

fieldnames1 = ['baseFee','gasUsed']

# Write data
with open('data/baseFee(05.02-06.02).csv', 'w') as file:
        csv_writer = csv.DictWriter(file, fieldnames=fieldnames1)
        csv_writer.writeheader()
        for i in range(len(base_fees)):
                csv_writer.writerow({'baseFee':base_fees[i], 'gasUsed':gas_used[i]})


