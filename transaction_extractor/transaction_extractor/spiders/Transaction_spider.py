import scrapy
from scrapy import Request
import time
import json
import csv
import os
import re

class Transaction(scrapy.Spider):
    api_key_main = 'BH2YA21A44751FCPN2HV3TAWGKQ35PP73H'
    name = "Transaction"
    block_rpc = 'https://api.etherscan.io/api?module=proxy&action=eth_getBlockByNumber&tag={}&boolean=true&apikey=TVB13TJQVKC67DEW16DMY1D6RRGJYYB5KK'
    tx_rpc = 'https://api.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&txhash={}&apikey=TVB13TJQVKC67DEW16DMY1D6RRGJYYB5KK'
    etherscan_url = 'https://etherscan.io/tx/{}'
    fieldnames = ['txHash', 'gasOffered', 'gasUsed', 'gasPrice', 'maxPriorityFee', 'maxFee', 'txTime']
    p = re.compile(r'\d+')
    DOWNLOAD_DELAY = 10
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Referer": "https://etherscan.io/",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-User": "?1",
        "Cache-Control": "max-age=0",
        "TE": "trailers"
    }

    cookies = {
        "_ga": "GA1.2.787687025.1633465707",
        "etherscan_cookieconsent": "True",
        "__stripe_mid": "321e3d49-6460-4fa4-b6a5-0d87ae60df3c21b445",
        "amp_fef1e8": "5be2c7b7-a35d-4c50-acd4-790ea621a527R...1fljo962m.1fljse1bs.1f.6.1l",
        "_ga_0JZ9C3M56S": "GS1.1.1638119283.2.1.1638125332.0",
        "ASP.NET_SessionId": "2zrjxwzmzsnm43bwujjwyzal",
        "__cflb": "02DiuFnsSsHWYH8WqVXbZzkeTrZ6gtmGUnJpdCjyBgGYk",
        "__cf_bm": "y6I55.gRhkJP.HylHwNdcYdsIpKQU0FEhCvwVCk7rRo-1638746180-0-AU+ZAXhiiXxw5riVLx9+rVim2DbJzT4O+c9SmOzxNp4OF6nnsGZPg+o/4RtzuQIC+jcJ6LKILEYS+xn9iFamPriHW1Sck3/+okhiphJ7q1XsBrTp5qNrvnTSMOhHXCBQLQ==",
        "_gid": "GA1.2.237746422.1638746181",
        "_gat_gtag_UA_46998878_6": "1"
    }

    def start_requests(self):
        with open('test.csv', 'w') as new_file:
            csv_writer = csv.DictWriter(new_file, fieldnames=self.fieldnames)
            csv_writer.writeheader()
            new_file.close()
        os.system('ls')
        tx_file = open('txs.txt')
        current_tx = tx_file.readline()
        test_count = 0
        while current_tx != '':
            yield Request(
                url=self.tx_rpc.format(current_tx),
                method='GET',
                dont_filter=True,
                cookies=Transaction.cookies,
                headers=Transaction.headers,
                callback=self.parse_trans
            )

            time.sleep(1)
            # test_count = test_count + 1
            current_tx = tx_file.readline()
            # if test_count == 10000:
            #     test_count = 0
            #     time.sleep(2)

        ### TEST
        # yield Request(
        #     url=self.tx_rpc.format('0x3d3a143f5670e34a60e775deffd3c9e9bf9298e5cc626906c5dfc92e058ccf66'),
        #     method='GET',
        #     dont_filter=True,
        #     cookies=Transaction.cookies,
        #     headers=Transaction.headers,
        #     callback=self.parse_trans
        # )

    def parse_trans(self, response):
        tx_obj = json.loads(response.body)
        # Tx hash ---> could be refactored
        try:
            tx_hash = tx_obj['result']['hash']
            if tx_hash == '':
                raise RuntimeError('Could not extract hash')
        except KeyError:
            return
        except RuntimeError as e:
            self.log(e)
            return

        # Gas units offered
        try:
            gas_offered = int(tx_obj['result']['gas'], base=16)
            if gas_offered == '':
                raise RuntimeError('Could not extract offered gas')
        except KeyError:
            return
        except RuntimeError as e:
            self.log(e)
            return

        # Gas price in wei
        try:
            gas_price = int(tx_obj['result']['gasPrice'], base=16)
            if gas_price == '':
                raise RuntimeError('Could not extract gas price')
        except KeyError:
            return
        except RuntimeError as e:
            self.log(e)
            return

        # Priority fee offered
        try:
            max_priority_fee_per_gas = int(tx_obj['result']['maxPriorityFeePerGas'], base=16)
        except KeyError:
            max_priority_fee_per_gas = None
        # Max fee per gas
        try:
            max_fee_per_gas = int(tx_obj['result']['maxFeePerGas'], base=16)
        except KeyError:
            max_fee_per_gas = None
        yield Request(
            url=self.etherscan_url.format(tx_hash),
            method='GET',
            dont_filter=True,
            cookies=Transaction.cookies,
            headers=Transaction.headers,
            callback=self.help_parse,
            meta={'txHash': tx_hash, 'gasOffered': gas_offered, 'gasPrice': gas_price,
                  'maxPrior': max_priority_fee_per_gas, 'maxFee': max_fee_per_gas}
        )

    def help_parse(self, response):
        if response.url == "https://etherscan.io/busy":
            return
        # Exec time
        try:
            tx_time = response.css('span[class="text-secondary ml-2 d-none d-sm-inline-block"]::text')[1].get().strip()
            tx_time = self.extract_time(self.p.findall(tx_time))
            if tx_time == '' or tx_time == 0:
                raise RuntimeError("Could not extract tx completion time")
        except RuntimeError as e:
            self.log(e)
            return
        # Gas Used in units
        try:
            gas_used_str = response.css('span[id="ContentPlaceHolder1_spanGasUsedByTxn"]::text').get()
            gas_used = gas_used_str.split('(')[0].strip()
            gas_used = gas_used.replace(',', '')
            if gas_used == '':
                raise RuntimeError("Could not extract gas used units")
        except RuntimeError as e:
            self.log(e)
            return

        tx_hash = response.meta.get('txHash')
        gas_offered = response.meta.get('gasOffered')
        gas_price = response.meta.get('gasPrice')
        max_prior = response.meta.get('maxPrior')
        max_fee = response.meta.get('maxFee')
        # Write to csv
        with open('data.csv', 'a') as file:
            csv_writer = csv.DictWriter(file, fieldnames=self.fieldnames)
            csv_writer.writerow({'txHash': tx_hash, 'gasOffered': gas_offered, 'gasUsed':gas_used, 'gasPrice': gas_price,
                                 'maxPriorityFee': max_prior, 'maxFee': max_fee, 'txTime': tx_time})
            file.close()

    # Helper func
    def extract_time(self, str):
        if len(str) == 1:
            return int(str[0])
        elif len(str) == 2:
            return int(str[0])*60 + int(str[1])
        elif len(str) == 3:
            return int(str[0])*3600 + int(str[1])*60 + int(str[2])
        else:
            return 0



