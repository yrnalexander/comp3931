import scrapy
from scrapy import Request
import time
import json
class EtherScan(scrapy.Spider):
    name = "Etherscan"
    txt_count = 0
    block_number_begin = 14114710
    block_number_end = 14108215
    # block_number_end = 14108215
    DOWNLOAD_DELAY = 10
    hrefs = []
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:92.0) Gecko/20100101 Firefox/92.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Referer": "https://etherscan.io/",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "same-origin",
        "Sec-Fetch-User": "?1",
        "Cache-Control": "max-age=0",
        "TE": "trailers"
    }

    cookies = {
        "_ga": "GA1.2.787687025.1633465707",
        "etherscan_cookieconsent": "True",
        "__stripe_mid": "321e3d49-6460-4fa4-b6a5-0d87ae60df3c21b445",
        "amp_fef1e8": "5be2c7b7-a35d-4c50-acd4-790ea621a527R...1fljo962m.1fljse1bs.1f.6.1l",
        "_ga_0JZ9C3M56S": "GS1.1.1638119283.2.1.1638125332.0",
        "ASP.NET_SessionId": "2zrjxwzmzsnm43bwujjwyzal",
        "__cflb": "02DiuFnsSsHWYH8WqVXbZzkeTrZ6gtmGUnJpdCjyBgGYk",
        "__cf_bm": "y6I55.gRhkJP.HylHwNdcYdsIpKQU0FEhCvwVCk7rRo-1638746180-0-AU+ZAXhiiXxw5riVLx9+rVim2DbJzT4O+c9SmOzxNp4OF6nnsGZPg+o/4RtzuQIC+jcJ6LKILEYS+xn9iFamPriHW1Sck3/+okhiphJ7q1XsBrTp5qNrvnTSMOhHXCBQLQ==",
        "_gid": "GA1.2.237746422.1638746181",
        "_gat_gtag_UA_46998878_6": "1"
    }



    def start_requests(self):
        urls = [
            # "https://etherscan.io/txs"
            'https://api.etherscan.io/api?module=proxy&action=eth_getBlockByNumber&tag={}&boolean=true&apikey=BH2YA21A44751FCPN2HV3TAWGKQ35PP73H'
        ]

        for url in urls:
            while self.block_number_begin > self.block_number_end:
                yield Request(
                    url=url.format(hex(self.block_number_begin)),
                    method='GET',
                    dont_filter=True,
                    cookies=EtherScan.cookies,
                    headers=EtherScan.headers,
                    callback=self.parse
                )
                time.sleep(1)
                self.block_number_begin = self.block_number_begin - 1
        # for tx_hash in self.hrefs:
        #     href = "https://etherscan.io/tx/" + tx_hash
        #     yield Request(
        #         url=href,
        #         method='GET',
        #         dont_filter=True,
        #         cookies=EtherScan.cookies,
        #         headers=EtherScan.headers,
        #         callback=self.parse_trans
        #     )
        #     time.sleep(1)

    def parse(self, response, **kwargs):
        file1 = open("txs.txt", "a")
        block_obj = json.loads(response.body)
        for i in range(len(block_obj['result']['transactions'])):
            self.hrefs.append(block_obj['result']['transactions'][i]['hash'])
            file1.write(block_obj['result']['transactions'][i]['hash'])
            file1.write('\n')
            # print(block_obj['result']['transactions'][i]['hash'])
        file1.close()
        # transactions = response.css('a[class="myFnExpandBox_searchVal"]::attr(href)').getall()
        # for transaction in transactions:
        #     href = "https://etherscan.io" + transaction
        #     yield Request(
        #         url=href,
        #         method='GET',
        #         dont_filter=True,
        #         cookies=EtherScan.cookies,
        #         headers=EtherScan.headers,
        #         callback=self.parse_trans
        #     )
        #     time.sleep(1)
        #     # print(href)
        #     # self.txt_count = self.txt_count + 1
        # print(len(self.hrefs))
    def parse_trans(self, response):
        if response.url == "https://etherscan.io/busy":
            return
        try:
            tx_time = response.css('span[class="text-secondary ml-2 d-none d-sm-inline-block"]::text')[1].get()
        except RuntimeError as e:
            self.log("Could not extract time")
            return





