import numpy as np
import matplotlib.pyplot as plt
from pandas import read_csv
import math

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM, Flatten
from tensorflow.keras import optimizers
from tensorflow.keras.backend import clear_session

from time import time

# Helper function to arrange x values(step length) and a y output
# Example: if we have data [[1],[2],[3],[4],[5],[6]] and a step 
# of 3  output will be x values : [[1,2,3],[2,3,4],[3,4,5]], y values : [4,5,6]
def mod_data(dataset, step):
    x = []
    y = []
    for i in range(len(dataset)-step):
        x.append(dataset[i:i+step,0])
        y.append(dataset[i+step,0])
    
    return np.array(x), np.array(y)



# Variables for error metrics
# RMSE - Root Mean Squared Error
# MAE - Mean Absolute Error
# R2 - Coefficient of determination
average_rmse_train = 0
average_mae_train = 0
average_r2_train = 0
average_time = 0

average_rmse_test = 0
average_mae_test = 0
average_r2_test = 0

average_mae_train_scaled = 0
average_mae_test_scaled = 0

# Number of times the model training experiment is ran
n = 2
for i in range(n):
    # Read data
    data = read_csv('baseFee(05.02-06.02).csv',usecols=[0])
    dataset = data.values
    dataset = dataset.astype('float32')
    # Scale data to be between 0-1
    scaler = MinMaxScaler(feature_range=(0,1))
    dataset = scaler.fit_transform(dataset)

    # Split data training(70%) Test data(30%)
    train_size = int(len(dataset)*0.7)
    test_size = len(dataset) - train_size

    train = dataset[0:train_size,:]
    test = dataset[train_size:len(dataset),:]

    # Define data step to arrange input/output vectors
    data_step = 100

    # Shape the input/output vectors 
    trainX, trainY = mod_data(train, data_step)
    testX, testY = mod_data(test, data_step)

    # Reshape data so it is in the right format for the network
    trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
    testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))

    # Initialize network
    model = Sequential()
    # 1 LSTM layer with 64 weights
    # Comment next line if more than 1 lstm units are required
    model.add(LSTM(64, input_shape=(None,data_step)))

    # Remove comment of next 2 lines for 2 LSTM units
    # model.add(LSTM(64,return_sequences=True,activation='tanh', input_shape=(None,data_step)))
    # model.add(LSTM(64, activation='tanh'))

    # Dense output layer 
    model.add(Dense(32))
    # Final output layer
    model.add(Dense(1))

    # Optimizer for back-propagation
    sgd = optimizers.SGD(lr=0.1, decay=1e-4, momentum=0.9)
    model.compile(loss='mean_squared_error', optimizer=sgd)
    model.summary()

    # Train
    start = time()
    model.fit(trainX, trainY, validation_data=(testX, testY), verbose=2, epochs=100)
    average_time = average_time + (time()-start)

    # Make predictions on train and test data
    trainPredict = model.predict(trainX)
    testPredict = model.predict(testX)

    # Reshape to calculate error metrics
    trainY = np.reshape(trainY, (1, trainY.shape[0]))
    testY = np.reshape(testY, (1, testY.shape[0]))

    # Compute Errors before inverse
    trainScore_rmse = math.sqrt(mean_squared_error(trainY[0], trainPredict[:,0]))
    testScore_rmse = math.sqrt(mean_squared_error(testY[0], testPredict[:,0]))

    trainScore_r2 = r2_score(trainY[0], trainPredict[:,0])
    testScore_r2 = r2_score(testY[0], testPredict[:,0])

    trainScore_mae = mean_absolute_error(trainY[0], trainPredict[:,0])
    testScore_mae = mean_absolute_error(testY[0], testPredict[:,0])

    # Reshape to prepare for inverse
    trainY = np.reshape(trainY, (trainY.shape[1]))
    testY = np.reshape(testY, (testY.shape[1]))

    # Inverse the data to original format 
    trainPredict = scaler.inverse_transform(trainPredict)
    trainY = scaler.inverse_transform([trainY])
    testPredict = scaler.inverse_transform(testPredict)
    testY = scaler.inverse_transform([testY])


    # Calculate the actual(scaled) MAE value 
    average_mae_train_scaled = average_mae_train_scaled + mean_absolute_error(trainY[0], trainPredict[:,0])
    average_mae_test_scaled = average_mae_test_scaled + mean_absolute_error(testY[0], testPredict[:,0])
    
    # Add up the errors to calculate an average of all (n) runs
    average_rmse_train = average_rmse_train + trainScore_rmse
    average_rmse_test = average_rmse_test + testScore_rmse
    
    average_r2_train = average_r2_train + trainScore_r2
    average_r2_test = average_r2_test + testScore_r2
    
    average_mae_train = average_mae_train + trainScore_mae
    average_mae_test = average_mae_test + testScore_mae
    

# Prepare results for plotting
trainPredictPlot = np.empty_like(dataset)
trainPredictPlot[:,:] = np.nan
trainPredictPlot[data_step:len(trainPredict)+data_step,:] = trainPredict

testPredictPlot = np.empty_like(dataset)
testPredictPlot[:,:] = np.nan
testPredictPlot[len(trainPredict)+(data_step*2):len(dataset), :] = testPredict

# Print error metrics to console
print("Average Train RMSE: {:.4f}".format(average_rmse_train/n))
print("Average Test RMSE: {:.4f}".format(average_rmse_test/n))

print("Average Train MAE: {:.4f}".format(average_mae_train/n))
print("Average Test MAE: {:.4f}".format(average_mae_test/n))

print("Average Train R2: {:.2f}".format(average_r2_train/n))
print("Average Test R2: {:.2f}".format(average_r2_test/n))

print("Average Time: {:.2f}".format(average_time/n))

print("Average Train MAE(Scaled): {:.4f}".format(average_mae_train_scaled/n))
print("Average Test MAE(Scaled): {:.4f}".format(average_mae_test_scaled/n))


# Plot data, predictions on training data, predictions on test data
plt.plot(scaler.inverse_transform(dataset))
plt.plot(trainPredictPlot)
plt.plot(testPredictPlot)

# Next lines are modified to arrange the plot legend 

# plt.title("Predicting baseFee - 1 LSTM unit",fontsize=14)
plt.title("Predicting maxPriorityFeePerGas - 1 LSTM unit",fontsize=14)
plt.xlabel("Block",fontsize=14)
# plt.ylabel("baseFee",fontsize=14)
plt.ylabel("maxPriorityFeePerGas",fontsize=14)

plt.plot(1, 20, "-b", label="Collected data")
plt.plot(1, 19, "orange", label="Prediction on train data")
plt.plot(1, 18, "-g", label="Prediction on test data")
plt.legend(loc="upper left")
plt.show()
