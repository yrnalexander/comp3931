# COMP3931

Repository for code deliverables maintained by Alexander Pitsin

# Project Structure

lstm folder - contains the lstm network and data files

scripts folder - contains web3 utility class under utility/ that is used to connect and gather information from a local Ethereum node,
test script under test/ for testing the utility class, block_data.py - collects the baseFeePerGas of blocks, fee_data.py - collects the maxPriorityFeePerGas, normalize.py - samples data by removing outliers, plot.py - used for different plots and data\ folder in where the output of block_data.py and fee_data.py is stored.

transaction_extractor - contains workings on the initial data collection methodology. 

requirements.txt files for each component that list the dependencies  required by the project.
